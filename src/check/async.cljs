(ns check.async
  (:require-macros [check.async])
  (:require [clojure.string :as str]
            [check.core :as core]
            [clojure.test :as test]
            [cljs.test]
            [clojure.core.async :as async]
            [promesa.core :as p]
            [cljs.core.async.impl.protocols :as proto]
            [promesa.protocols :as promesa-proto]))

(defn to-promise [promise-or-chan]
  (if (satisfies? proto/ReadPort promise-or-chan)
    (let [p (p/deferred)]
      (async/go
        (let [res (async/<! promise-or-chan)]
          (p/resolve! p res)))
      p)
    promise-or-chan))

(defrecord AsyncTest [prom]
  cljs.test/IAsyncTest
  cljs.core/IFn
  (-invoke [_ done] (p/finally prom (fn [ & _] (done))))
  promesa-proto/IPromise
  (-bind [_ f] (promesa-proto/-bind prom f))
  (-bind [_ f executor] (promesa-proto/-bind prom f executor))
  (-map [_ f] (promesa-proto/-map prom f))
  (-map [_ f executor] (promesa-proto/-map prom f executor))
  (-then [_ f] (promesa-proto/-then prom f))
  (-then [_ f executor] (promesa-proto/-then prom f executor))
  (-mapErr [_ f] (promesa-proto/-mapErr prom f))
  (-mapErr [_ f executor] (promesa-proto/-mapErr prom f executor))
  (-thenErr [_ f] (promesa-proto/-thenErr prom f))
  (-thenErr [_ f executor] (promesa-proto/-thenErr prom f executor))
  (-handle [_ f] (promesa-proto/-handle prom f))
  (-handle [_ f executor] (promesa-proto/-handle prom f executor))
  (-finally [_ f] (promesa-proto/-finally prom f))
  (-finally [_ f executor] (promesa-proto/-finally prom f executor)))

(defn async-test* [description timeout teardown prom]
  (let [timeout-error (js/Object.)
        error (js/Object.)
        promise (p/catch prom #(vector error %))
        timeout-prom (p/do!
                      (p/delay timeout)
                      timeout-error)

        full-prom
        (-> (p/race [promise timeout-prom])
            (p/then (fn [res]
                      (cond
                        (and (vector? res) (-> res first (= error)))
                        (do
                          (test/do-report {:type :error
                                           :expected "Not an error"
                                           :actual (-> res second core/normalize-error)}))

                        (= timeout-error res)
                        (test/do-report {:type :error
                                         :expected "Test to finish"
                                         :actual (core/normalize-error
                                                  (str "Test did ""not finish in "
                                                       timeout "msec"))}))

                      (teardown)))
            (p/catch (fn [error] (test/is (not error)) nil)))]
    (->AsyncTest full-prom)))
