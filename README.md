# Check

Library helpers for a better testing world.

## Motivation
Clojure's default test library is... well... limited, to say the least. There are better options out there, but `expectations` is too opinated in "one assertion per test", `midje` is **way too magic** (and also don't work on ClojureScript), `speclj` is probably dead (and don't work with async tests).

So, enters this library: it wraps `expectations` and `matcher-combinators` so we're not trying to reinvent the wheel, uses `promesa` to handle ClojureScript async tests (and to keep the same API between Clojure and ClojureScript), and uses "midje-style" arrows **or** Clojure `is`-style match.

## Usage

Add check to your project's dependencies:

[![Clojars Project](https://img.shields.io/clojars/v/check.svg)](https://clojars.org/check)

You can use the "arrow" expectations the same way you would use `matcher-combinators` lib:

```clojure
(require '[check.core :refer [check]]
         '[clojure.test :refer [deftest]])

(deftest ten-is-even
  (check 10 => even?))

;; Or, if you prefer to keep close to Clojure's is:
(deftest ten-is-even-other-style
  (check (=> even? 10)))

(deftest regexp-test
  (check "some string" => #"str"))

;; You can use `in` like you would use in expectations...
(require '[expectations :refer [in]])
(deftest in-vector
  (check (in [1 2 3 4]) =expect=> 3)
  ; OR
  (check (=expect=> 3 (in [1 2 3 4]))))

;; Or, to avoid another require, you can use =includes=>
(deftest in-with-includes
  (check [1 2 3 4] =includes=> 3)
  ; OR
  (check (=includes=> 3 [1 2 3 4])))
```

### Async tests

You can use `async-test` to generate a test that will timeout after a while, and have a teardown so you can clean up connections/resources/etc:

```clojure
; Please notice that there's a `check` that works only for async functions:
(require '[check.async :refer [async-test check]]
         '[promesa.core :as p])

(deftest some-test
  (async-test "checks for async code"
    (let [elem (p/do!
                 (p/delay 400)
                 :done)]
      (check elem => :done)
      ; OR
      (check (=> :done elem)))))
```

**Notice** - if you decide for an async test, there are some small things to consider:

Remember that, in ClojureScript, the _runtime itself_ is async. There's no _await_ that will block the main thread. This means, for all purposes, that you HAVE TO BE SURE that you're returning **a single promise** that awaits for all elements.

`async-test` itself wraps around `promesa.core/do!`. This awaits all promises _that appear on top level_ - it's the same as this Javascript code:

```javascript
(async function() {
  await first_code();
  await second_code();
  await ....
})();
```

So, if you have an async API, you'll have to "await" every binding and also the body. The namespace `check.async` offers an API for that - below is an example of the most complex an async test can be, with multiple awaits, bindings, teardown an a greater timeout:

```clojure
(require '[check.async :refer [async-test check testing let-testing]]
         '[promesa.core :as p]
         '[matcher-combinators.matchers :as m])

(def db-conn (atom nil))
(deftest complex-test
  (async-test "queries and updated the DB"
    {:timeout 4000, :teardown (some-> @db-conn db/stop!)}

    (let-testing "connects correctly to the DB" [conn (db/connect!)]
      (reset! db-con conn)
      (check (db/query! @db-conn "SELECT 1 ok") => [{:ok 1}]))

    (testing "saves some records on DB"
      (check (db/create-person! @db-conn [{:name "Andrew"}])
             => {:records 1 :ids [number?]})

      (check (db/create-person! @db-conn [{:name "Samantha"}
                                          {:name "Raphael"}])
             => {:records 1}))

    (testing "queries people"
      (check (db/find-all-people! @db-conn)
             => (m/in-any-order [{:name "Samantha"}
                                 {:name "Andrew"}
                                 {:name "Raphael"}])))))
```

Please notice that:

1. I'm assuming **every function** in the `db` namespace returns a promise;
1. You can use both APIs here: I'm using `(check <actual> => <expected>)` but it's possible to also use `(check (=> <expected> <actual>))`. Custom matchers also work fine;
1. Although everything is async, the test **runs synchronously**. So it does what we expect (connects, inserts data, then queries them)
1. `check.async/check` itself return a promise, so it can be composed if you need (probably you don't, but who knows?)

## Extending
Suppose you have a very complicated map that represents some internal structure of your code. You probably don't want to keep repeating your map data all over the place, and the matchers that are included are not sufficient. `check` allows you to create custom matchers, so you have a custom way of matching your data to your expectation, and also a custom error to guide you to find the problem in your code.

For example, suppose you have a map in the format:
```clojure
{:accounts [{:name "Savings" :amount 200M}
            {:name "Primary" :amount 20M}]}
```

And you want to check the amount a person have in each account, but you don't want to "tie" your implementation to your "map shape". You can write a checker like this:

```clojure
;; First we define a way to compare what we expect with our current data:
(defn- mismatches [accounts [name amount]]
  (if-let [acc (->> accounts
                    (filter #(-> % :name (= name)))
                    first)]
    (if (= amount (:amount acc))
      nil
      (str "Account " name " should have " amount ", but have " (:amount acc)))
    (str "Account " name " isn't present on list of accounts")))

;; Then we define our matcher, that will call this function to check for problems:
(check/defmatcher =have-amount=> [expected actual]
  (let [accs (:accounts actual)
        misses (map #(mismatches accs %) expected)
        ;; We remove rows where there's no problem at all
        misses (remove nil? misses)]
    {:pass? (empty? misses) ;; If pass? is true, failure-message is not needed
     :failure-message (str "\n" (clojure.string/join "\n" misses))}))

;; Then, on some test:
(deftest check-accounts
  (check {:accounts [{:name "Savings" :amount 200M}
                     {:name "Primary" :amount 20M}]}
         =have-amount=> {"Savings" 100M
                         "Investments" 1000M}))

;; This will fail with:
;FAIL in (check-accounts) (at test.clj:4:4)
;expected: {"Savings" 100, "Investments" 1000}
;  actual:
;Account Savings should have 100, but have 200
;Account Investments isn't present on list of accounts
```

## Mocks and Stubs
Currently, there's a `mocking` macro that allows you to mock some requests. Currently, only stubs are supported - something that will stub your global vars and return a value. Support for mocks, spies, etc is planned.

```clojure
(require '[check.core :refer [check]]
         '[check.mocks :refer [mocking]])

;; You define your mocks with `mocking` macro:

(deftest some-test
  (mocking
    (http/get "http://localhost:8000") => {:body "Hello, world!"}
    ; Simulating different requests every code
    (http/post "http://localhost:8000" {:foo "BAR"}) =streams=> [:ok :fail]
    ---
    (check (http/get "http://localhost:8000") => {:body string?})
    (check (http/post "http://localhost:8000") => :ok)
    (check (http/post "http://localhost:8000") => :fail)
```

Mocks work both with sync and async tests. If you want to use on async tests, just put `mocking` before `async-test` or before anything that returns promises, such as an async function, or `check.async/testing` or `check.async/let-testing`. See [the tests](test/check/mocks_test.cljc) for more examples.

## Rationale

A test should not depend on internal structure of the data. If you chose to represent the current permission of a user as a bitmap, or as a vector, or even a string, shouldn't matter - the test should keep passing (or failing) if no change of behavior happens.

At the same time, a test should not simply say that "it failed". It should guide you to the problem if possible.

Some tools already help with the second part - matcher combinators, expectations, etc. But they are complicated (or impossible) to extend. So you either use these tools as-is (and is forced to match on internal data) or you define a preditacte that returns true or false, and lose the power to get a detailed error.

Then, there's ClojureScript and async testing. I honestly can't think on how someone can test async code in ClojureScript. If you define an async test, and never call done, your test will hang forever. You don't have a "timeout" option; you can't use try/catch, because it's asynchronous; you don't have teardown, because you don't have try/catch. You could use fixtures, but they are global and also complicated to use (and, as done is never called in some situations, the teardown code never runs). Worse yet, you can't mock with `with-redefs` because, again, it asynchronous. And, of you have a CLJC code, you can't write a test for both Clojure and ClojureScript...

Check comes to solve all these issues. It contains custom matchers that are extensible with `defmatcher`. It have mocks that work on async code. It defines an api, `async-test`, that works both with Clojure and ClojureScript, that support a configurable timeout **and** teardown. Matchers, and custom matchers, have the option to explain what's wrong, so you can know what's happening under the hood. They are also color-coded, so you can visually see what need to be changed.

Check also contains an API that's close to `is` macro, and one that's close to midje arrows. Both works as expected, and they are less "magical" than midje was - it's easier to understand what's going on if one needs to hack (or port) to other runtime.

And finally, async tests run with Javascript promises, not core.async channels. This means that you'll be closer to the ClojureScript environment, and that errors are captured by promises, so they'll not blow up the Javascript runtime in node.js.

## License

Copyright © 2018 Maurício Szabo

Distributed under the Eclipse Public License either version 1.0 or any later version.
