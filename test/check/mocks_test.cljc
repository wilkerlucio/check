(ns check.mocks-test
  (:require [clojure.test :refer [deftest testing]]
            [check.core :refer [check]]
            [check.async :as async]
            [promesa.core :as p]
            [check.mocks :refer [mocking]]
            [net.cgrand.macrovich :as macros]))

(defn some-function [a b]
  (+ a b))

(deftest mocking-some-function
  (testing "mocking a single call of a function"
    (mocking
     (some-function 10 20) => 10
     ---
     (check (some-function 10 20) => 10)))

  (testing "error if mock don't agree"
    (mocking
     (some-function 1 2) => 10
     ---
     (check (some-function 0 0) =throws=> #?(:clj clojure.lang.ExceptionInfo
                                             :cljs ExceptionInfo)))))

(deftest mocking-more-than-one-arg
  (testing "mocking a single call of a function"
    (mocking
     (some-function 10 20) => 10
     (some-function 0 0) => 9
     ---
     (check (some-function 10 20) => 10)
     (check (some-function 0 0) => 9))))

(deftest mocking-side-effects
  (testing "mocking a single call of a function"
    (mocking
     (some-function 10 20) =streams=> [1 2]
     ---
     (check (some-function 10 20) => 1)
     (check (some-function 10 20) => 2)
     (check (some-function 10 20) =throws=> #?(:clj clojure.lang.ExceptionInfo
                                                :cljs ExceptionInfo)))))

(deftest mocking-on-async-tests
  (mocking
   (some-function 10 20) => 0
   ---
   (async/async-test "mocking async"
     (p/delay 200)
     (check (some-function 10 20) => 0))))

(deftest mocking-on-async-funs
  (async/async-test "mocking async fun"
    (async/check (mocking
                  (some-function 20 30) => -1
                  ---
                  (p/do!
                   (p/delay 200)
                   (some-function 20 30)))
           => -1)))

(deftest mocking-on-async-testing-subelems
  (async/async-test "mocking async `testing` elements"
    (mocking
     (some-function 1 2) => 9
     ---
     (async/check (some-function 1 2) => 9))

    (mocking
     (some-function 1 2) => 10
     ---
     (async/testing "here it should be 10"
       (async/check (some-function 1 2) => 10)))

    (async/testing "deep mocking"
      (mocking (some-function 1 2) => 11
       ---
       (async/check (some-function 1 2) => 11))

      (mocking (some-function 1 2) => 12
       ---
       (async/check (some-function 1 2) => 12)))))
